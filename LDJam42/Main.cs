﻿using LDJam42.Systems;
using LDJam42.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LDJam42
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        public Camera Camera { get; set; }
        public EntityManager EntityManager { get; set; }
        public GameBoard GameBoard { get; set; }

        public InputSystem MovementSystem { get; set; }
        public CollisionSystem CollisionSystem { get; set; }
        public RenderSystem RenderSystem { get; set; }
        public ObjectiveSystem ObjectiveSystem { get; set; }
        
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            Camera = new Camera(GraphicsDevice.Viewport);
            EntityManager = new EntityManager(Content);
            GameBoard = new GameBoard(Content, EntityManager, Camera);
            MovementSystem = new InputSystem(this);
            CollisionSystem = new CollisionSystem(this);
            ObjectiveSystem = new ObjectiveSystem(this);
            RenderSystem = new RenderSystem(this);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                //Exit();
            base.Update(gameTime);
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            MovementSystem.Update(delta);
            CollisionSystem.Update(delta);
            ObjectiveSystem.Update(delta);
            EntityManager.Update(delta);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);

            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, Camera.ViewMatrix());
            GameBoard.Draw(spriteBatch);
            RenderSystem.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
