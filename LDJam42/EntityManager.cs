﻿using LDJam42.Components;
using LDJam42.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace LDJam42
{
    public class EntityManager
    {
        private readonly ContentManager content;
        private int id = 0;
        public List<Entity> Entities { get; private set; }

        public EntityManager(ContentManager content)
        {
            Entities = new List<Entity>();
            this.content = content;
        }

        public void AddPlayer(int x, int y)
        {
            Entity entity = new Entity(id++);
            Transform transform = new Transform(entity)
            {
                X = x,
                Y = y,
                Width = 32,
                Height = 32,
            };
            entity.AddComponent(transform);
            Velocity velocity = new Velocity(entity)
            {
                X = 0,
                Y = 0
            };
            entity.AddComponent(velocity);
            Sprite sprite = new Sprite(entity)
            {
                Texture = content.Load<Texture2D>("player"),
                Color = Color.White,
                Scale = 1f,
                Origin = Vector2.Zero,
                IsAtlas = true,
                AtlasTiles = new Dictionary<string, Point>()
                {
                    { "PlayerDown",     new Point(0, 0) },
                    { "PlayerRight",    new Point(1, 0) },
                    { "PlayerUp",       new Point(0, 1) },
                    { "PlayerLeft",     new Point(1, 1) },
                    { "PlayerFalling",  new Point(2, 0) }
                },
                CurrentTile = "PlayerDown"
            };
            entity.AddComponent(sprite);
            Input input = new Input(entity)
            {
                ProcessingInput = false,
                Falling = false,
                Timer = 60,
                CurrenTime = 0,
                LockedInput = false,
                IsClickable = false,
                CheckCollisions = true,
                ClearedLevel = false,
                RecentlyToggledButton = false
            };
            entity.AddComponent(input);
            Player player = new Player(entity);
            entity.AddComponent(player);
            Entities.Add(entity);
        }

        public void AddMenu(GameBoard gameBoard)
        {
            AddMenuItem(112, 168, 64, 64, "One", new Point(0, 0), Color.Green, gameBoard.GenerateLevelOne);
            AddMenuItem(176, 168, 64, 64, "Two", new Point(1, 0), Color.Green, gameBoard.GenerateLevelTwo);
            AddMenuItem(240, 168, 64, 64, "Three", new Point(2, 0), Color.Green, gameBoard.GenerateLevelThree);
            AddMenuItem(304, 168, 64, 64, "Four", new Point(0, 1), Color.Yellow, gameBoard.GenerateLevelFour);
            AddMenuItem(368, 168, 64, 64, "Five", new Point(1, 1), Color.Yellow, gameBoard.GenerateLevelFive);
            AddMenuItem(432, 168, 64, 64, "Six", new Point(2, 1), Color.Yellow, gameBoard.GenerateLevelSix);
            AddMenuItem(496, 168, 64, 64, "Seven", new Point(0, 2), Color.Red, gameBoard.GenerateLevelSeven);
            AddMenuItem(560, 168, 64, 64, "Eight", new Point(1, 2), Color.Red, gameBoard.GenerateLevelEight);
            AddMenuItem(624, 168, 64, 64, "Nine", new Point(2, 2), Color.Red, gameBoard.GenerateLevelNine);
        }

        public void AddMenuItem(int x, int y, int width, int height, string atlasTile, Point uv, Color color, Action onClick)
        {
            Entity entity = new Entity(id++);
            Transform transform = new Transform(entity)
            {
                X = x,
                Y = y,
                Width = width,
                Height = height,
            };
            entity.AddComponent(transform);
            Sprite sprite = new Sprite(entity)
            {
                Texture = content.Load<Texture2D>("Menu"),
                Color = color,
                Scale = 1f,
                Origin = Vector2.Zero,
                IsAtlas = true,
                AtlasTiles = new Dictionary<string, Point>()
                {
                    { atlasTile, uv },
                },
                CurrentTile = atlasTile
            };
            entity.AddComponent(sprite);
            Input input = new Input(entity)
            {
                ProcessingInput = false,
                Falling = false,
                Timer = 0,
                CurrenTime = 0,
                LockedInput = false,
                IsClickable = true,
                OnClick = onClick,
                CheckCollisions = false,
                RecentlyToggledButton = false
            };
            entity.AddComponent(input);
            Entities.Add(entity);
        }

        public void Update(float delta)
        {
            for(int i = 0; i < Entities.Count; i++)
            {
                if (Entities[i].Killed)
                {
                    Entities.RemoveAt(i);
                }
            }
        }
    }
}
