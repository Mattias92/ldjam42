﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42
{
    public abstract class Component
    {
        public Entity Owner { get; set; }

        public Component(Entity owner)
        {
            Owner = owner;
        }

        public void Kill()
        {
            Owner.RemoveComponent(this);
        }
    }
}
