﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Components
{
    public class Velocity : Component
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float PreviousX { get; set; }
        public float PreviousY { get; set; }

        public Vector2 Vector2
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        public Vector2 PreviousVector2
        {
            get
            {
                return new Vector2(PreviousX, PreviousY);
            }
            set
            {
                PreviousX = value.X;
                PreviousY = value.Y;
            }
        }

        public Velocity(Entity owner) : base(owner)
        {
        }
    }
}
