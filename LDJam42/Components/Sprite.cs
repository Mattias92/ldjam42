﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Components
{
    public class Sprite : Component
    {
        public Texture2D Texture { get; set; }
        public Color Color { get; set; }
        public Vector2 Origin { get; set; }
        public float Scale { get; set; }
        public bool IsAtlas { get; set; }
        public string CurrentTile { get; set; }
        public Dictionary<string, Point> AtlasTiles { get; set; }

        public Sprite(Entity owner) : base(owner)
        {

        }
    }
}
