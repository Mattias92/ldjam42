﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Components
{
    public class Transform : Component
    {
        public float X { get; set; }
        public float Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle((int)X, (int)Y, Width, Height);
            }
            private set { }
        }

        public Vector2 Vector2
        {
            get
            {
                return new Vector2(X, Y);
            }
            private set { }
        }

        public Transform(Entity owner) : base(owner)
        {
            
        }
    }
}
