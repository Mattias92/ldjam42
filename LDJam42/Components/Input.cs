﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Components
{
    public class Input : Component
    {
        public MouseState mouseState { get; set; }
        public MouseState previousMouseState { get; set; }
        public KeyboardState keyboardState { get; set; }
        public KeyboardState previousKeyboardState { get; set; }
        public bool ProcessingInput { get; set; }
        public bool LockedInput { get; set; }
        public Vector2 OldPosition { get; set; }
        public Vector2 NextPosition { get; set; }
        public bool RecentlyToggledButton { get; set; }
        public bool Falling { get; set; }
        public bool ClearedLevel { get; set; }
        public int Timer { get; set; }
        public int CurrenTime { get; set; }
        public bool IsClickable { get; set; }
        public bool CheckCollisions { get; set; }
        public Action OnClick { get; set; }

        public Input(Entity owner) : base(owner)
        {
            
        }
    }
}
