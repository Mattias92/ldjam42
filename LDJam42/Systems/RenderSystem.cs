﻿using LDJam42.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Systems
{
    public class RenderSystem
    {
        private readonly EntityManager entityManager;

        public RenderSystem(Main main)
        {
            entityManager = main.EntityManager;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Entity e in entityManager.Entities)
            {
                if (e.HasComponent<Transform>() && e.HasComponent<Sprite>())
                {
                    Transform transform = e.GetComponent<Transform>();
                    Sprite sprite = e.GetComponent<Sprite>();
                    if (sprite.IsAtlas)
                    {
                        Point uv = sprite.AtlasTiles[sprite.CurrentTile];
                        spriteBatch.Draw(sprite.Texture, new Rectangle((int)transform.X, (int)transform.Y, transform.Width, transform.Height), new Rectangle(uv.X * 32, uv.Y * 32, 32, 32), sprite.Color, 0f, sprite.Origin, SpriteEffects.None, 0f);
                    }
                    else
                    {
                        spriteBatch.Draw(sprite.Texture, transform.Vector2, sprite.Color);
                    }
                }
            }
        }
    }
}
