﻿using LDJam42.Components;
using LDJam42.World;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Systems
{
    public class CollisionSystem
    {
        private readonly EntityManager entityManager;
        private readonly GameBoard gameBoard;

        public CollisionSystem(Main main)
        {
            entityManager = main.EntityManager;
            gameBoard = main.GameBoard;
        }

        public void Update(float delta)
        {
            foreach (Entity e in entityManager.Entities)
            {
                if (e.HasComponent<Transform>() && e.HasComponent<Input>())
                {
                    Transform transform = e.GetComponent<Transform>();
                    Input input = e.GetComponent<Input>();
                    Sprite sprite = e.GetComponent<Sprite>();

                    if (input.CheckCollisions)
                    {
                        Point tile = new Point((int)transform.X / 32, (int)transform.Y / 32);

                        if (!input.ProcessingInput)
                        {
                            // See if player has fallen off.
                            if (!gameBoard.HasFloor(tile.X, tile.Y))
                            {
                                input.Falling = true;
                                input.LockedInput = true;
                                if (sprite != null)
                                {
                                    sprite.CurrentTile = "PlayerFalling";
                                }
                            }

                            if (!gameBoard.HasFriction(tile.X, tile.Y))
                            {
                                if (e.HasComponent<Velocity>())
                                {
                                    input.ProcessingInput = true;
                                    input.OldPosition = transform.Vector2;
                                    Velocity velocity = e.GetComponent<Velocity>();
                                    if (velocity.PreviousX > 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X + 32, transform.Y);
                                        velocity.Vector2 = velocity.PreviousVector2;
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerRight";
                                        }
                                    }
                                    else if (velocity.PreviousX < 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X - 32, transform.Y);
                                        velocity.Vector2 = velocity.PreviousVector2;
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerLeft";
                                        }
                                    }
                                    else if (velocity.PreviousY > 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X, transform.Y + 32);
                                        velocity.Vector2 = velocity.PreviousVector2;
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerDown";
                                        }
                                    }
                                    else if (velocity.PreviousY < 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X, transform.Y - 32);
                                        velocity.Vector2 = velocity.PreviousVector2;
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerUp";
                                        }
                                    }
                                }
                            }

                            if (gameBoard.IsButton(tile.X, tile.Y))
                            {
                                if (!input.RecentlyToggledButton)
                                {
                                    input.RecentlyToggledButton = true;
                                    gameBoard.ToggleFloors();
                                }
                            }

                            if (gameBoard.HasVelocity(tile.X, tile.Y))
                            {
                                if (e.HasComponent<Velocity>())
                                {
                                    Velocity velocity = e.GetComponent<Velocity>();
                                    velocity.Vector2 = gameBoard.GetVelocity(tile.X, tile.Y);
                                    input.ProcessingInput = true;
                                    input.OldPosition = transform.Vector2;
                                    if (velocity.X > 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X + 32, transform.Y);
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerRight";
                                        }
                                    }
                                    else if (velocity.X < 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X - 32, transform.Y);
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerLeft";
                                        }
                                    }
                                    else if (velocity.Y > 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X, transform.Y + 32);
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerDown";
                                        }
                                    }
                                    else if (velocity.Y < 0)
                                    {
                                        input.NextPosition = new Vector2(transform.X, transform.Y - 32);
                                        if (sprite != null)
                                        {
                                            sprite.CurrentTile = "PlayerUp";
                                        }
                                    }
                                }
                            }

                            if (input.Falling)
                            {
                                input.CurrenTime++;
                                if (sprite != null)
                                {
                                    if (input.CurrenTime % 2 == 0)
                                    {
                                        transform.Width -= 1;
                                        transform.Height -= 1;
                                        transform.X += 0.5f;
                                        transform.Y += 0.5f;
                                    }
                                }
                                if (input.CurrenTime == input.Timer)
                                {
                                    gameBoard.GenerateLevel();
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
