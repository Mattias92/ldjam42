﻿using LDJam42.Components;
using LDJam42.World;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.Systems
{
    public class ObjectiveSystem
    {
        private readonly EntityManager entityManager;
        private readonly GameBoard gameBoard;

        public ObjectiveSystem(Main main)
        {
            entityManager = main.EntityManager;
            gameBoard = main.GameBoard;
        }

        public void Update(float delta)
        {
            foreach (Entity e in entityManager.Entities)
            {
                if (e.HasComponent<Player>() && e.HasComponent<Input>())
                {
                    Input input = e.GetComponent<Input>();
                    Player player = e.GetComponent<Player>();
                    
                    if (!input.ProcessingInput && !input.LockedInput) 
                    {
                        if (gameBoard.FloorsLeft <= 0)
                        {
                            input.ClearedLevel = true;
                            gameBoard.GenerateNextLevel();
                            return;
                        }
                    }
                }
            }
        }
    }
}
