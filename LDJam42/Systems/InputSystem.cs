﻿using LDJam42.Components;
using LDJam42.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LDJam42.Systems
{
    public class InputSystem
    {
        private readonly EntityManager entityManager;
        private readonly GameBoard gameBoard;

        public InputSystem(Main main)
        {
            entityManager = main.EntityManager;
            gameBoard = main.GameBoard;
        }

        private Point pointer;

        public void Update(float delta)
        {
            foreach (Entity e in entityManager.Entities)
            {
                if (e.HasComponent<Input>() && e.HasComponent<Transform>())
                {
                    Input input = e.GetComponent<Input>();
                    Velocity velocity = e.GetComponent<Velocity>();
                    Transform transform = e.GetComponent<Transform>();
                    Sprite sprite = e.GetComponent<Sprite>();

                    input.mouseState = Mouse.GetState();
                    input.keyboardState = Keyboard.GetState();

                    int x = input.mouseState.X;
                    int y = input.mouseState.Y;
                    pointer = new Point(x, y);

                    if (input.keyboardState.IsKeyDown(Keys.Escape))
                    {
                        gameBoard.ExitLevel();
                        return;
                    }

                    // If no input, await new input.
                    if (!input.ProcessingInput && !input.LockedInput)
                    {
                        if (velocity != null)
                        {
                            if (input.keyboardState.IsKeyDown(Keys.Down))
                            {
                                if (!gameBoard.HasCollision((int)transform.X / 32, ((int)transform.Y / 32) + 1))
                                {
                                    velocity.Y = 2f;
                                    input.OldPosition = transform.Vector2;
                                    input.NextPosition = new Vector2(transform.X, transform.Y + 32);
                                    input.ProcessingInput = true;
                                    if (sprite != null)
                                    {
                                        sprite.CurrentTile = "PlayerDown";
                                    }
                                }
                            }
                            else if (input.keyboardState.IsKeyDown(Keys.Up))
                            {
                                if (!gameBoard.HasCollision((int)transform.X / 32, ((int)transform.Y / 32) - 1))
                                {
                                    velocity.Y = -2f;
                                    input.OldPosition = transform.Vector2;
                                    input.NextPosition = new Vector2(transform.X, transform.Y - 32);
                                    input.ProcessingInput = true;
                                    if (sprite != null)
                                    {
                                        sprite.CurrentTile = "PlayerUp";
                                    }
                                }
                            }
                            else if (input.keyboardState.IsKeyDown(Keys.Right))
                            {
                                if (!gameBoard.HasCollision(((int)transform.X / 32) + 1, (int)transform.Y / 32))
                                {
                                    velocity.X = 2f;
                                    input.OldPosition = transform.Vector2;
                                    input.NextPosition = new Vector2(transform.X + 32, transform.Y);
                                    input.ProcessingInput = true;
                                    if (sprite != null)
                                    {
                                        sprite.CurrentTile = "PlayerRight";
                                    }
                                }
                            }
                            else if (input.keyboardState.IsKeyDown(Keys.Left))
                            {
                                if (!gameBoard.HasCollision(((int)transform.X / 32) - 1, (int)transform.Y / 32))
                                {
                                    velocity.X = -2f;
                                    input.OldPosition = transform.Vector2;
                                    input.NextPosition = new Vector2(transform.X - 32, transform.Y);
                                    input.ProcessingInput = true;
                                    if (sprite != null)
                                    {
                                        sprite.CurrentTile = "PlayerLeft";
                                    }
                                }
                            }
                        }

                        if (input.mouseState.LeftButton == ButtonState.Pressed)
                        {
                            if (input.IsClickable)
                            {
                                if (transform.Rectangle.Contains(pointer))
                                {
                                    input.OnClick.Invoke();
                                    return;
                                }
                            }
                        }
                    }

                    // Else process input.
                    else if (velocity != null)
                    {
                        float distanceLeft = 0;

                        // Move Horizontal
                        if (velocity.X != 0)
                        {
                            if (transform.X > input.NextPosition.X)
                            {
                                distanceLeft = transform.X - input.NextPosition.X;
                                if (velocity.X > distanceLeft)
                                {
                                    velocity.X = distanceLeft;
                                }
                            }
                            else if (input.NextPosition.X > transform.X)
                            {
                                distanceLeft = input.NextPosition.X - transform.X;
                                if (velocity.X > distanceLeft)
                                {
                                    velocity.X = distanceLeft;
                                }
                            }

                            if (distanceLeft != 0)
                            {
                                transform.X += velocity.X;
                            }
                            else
                            {
                                input.ProcessingInput = false;
                                velocity.PreviousX = velocity.X;
                                velocity.PreviousY = 0;
                                velocity.X = 0;
                                gameBoard.DestroyFloor((int)input.OldPosition.X / 32, (int)input.OldPosition.Y / 32);
                                gameBoard.DamageFloor((int)transform.X / 32, (int)transform.Y / 32);
                                input.RecentlyToggledButton = false;
                            }
                        }

                        // Move Vertically
                        if (velocity.Y != 0)
                        {
                            if (transform.Y > input.NextPosition.Y)
                            {
                                distanceLeft = transform.Y - input.NextPosition.Y;
                                if (velocity.Y > distanceLeft)
                                {
                                    velocity.Y = distanceLeft;
                                }
                            }
                            else if (input.NextPosition.Y > transform.Y)
                            {
                                distanceLeft = input.NextPosition.Y - transform.Y;
                                if (velocity.Y > distanceLeft)
                                {
                                    velocity.Y = distanceLeft;
                                }
                            }

                            if (distanceLeft != 0)
                            {
                                transform.Y += velocity.Y;
                            }
                            else
                            {
                                input.ProcessingInput = false;
                                velocity.PreviousX = 0;
                                velocity.PreviousY = velocity.Y;
                                velocity.Y = 0;
                                gameBoard.DestroyFloor((int)input.OldPosition.X / 32, (int)input.OldPosition.Y / 32);
                                gameBoard.DamageFloor((int)transform.X / 32, (int)transform.Y / 32);
                                input.RecentlyToggledButton = false;
                            }
                        }
                    }

                    input.previousMouseState = input.mouseState;
                    input.previousKeyboardState = input.keyboardState;
                }
            }
        }
    }
}
