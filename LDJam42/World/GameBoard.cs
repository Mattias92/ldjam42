﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.World
{
    public class GameBoard
    {
        private readonly ContentManager content;
        private readonly EntityManager entityManager;
        private readonly Camera camera;
        private Texture2D texture;
        private int width = 8;
        private int height = 8;

        public Tile[,] Tiles;
        public bool PlayingLevel { get; set; }
        public int CurrentLevel { get; set; }
        public int FloorsLeft { get; set; }

        public GameBoard(ContentManager content, EntityManager entityManager, Camera camera)
        {
            this.camera = camera;
            this.content = content;
            this.entityManager = entityManager;
            entityManager.AddMenu(this);
            PlayingLevel = false;
        }

        public void GenerateNextLevel()
        {
            CurrentLevel++;
            GenerateLevel();
        }

        public void GenerateLevel()
        {
            entityManager.Entities.Clear();
            switch (CurrentLevel)
            {
                case 1:
                    GenerateLevelOne();
                    break;
                case 2:
                    GenerateLevelTwo();
                    break;
                case 3:
                    GenerateLevelThree();
                    break;
                case 4:
                    GenerateLevelFour();
                    break;
                case 5:
                    GenerateLevelFive();
                    break;
                case 6:
                    GenerateLevelSix();
                    break;
                case 7:
                    GenerateLevelSeven();
                    break;
                case 8:
                    GenerateLevelEight();
                    break;
                case 9:
                    GenerateLevelNine();
                    break;
                default:
                    ExitLevel();
                    break;
            }
        }

        public void ExitLevel()
        {
            entityManager.Entities.Clear();
            entityManager.AddMenu(this);
            PlayingLevel = false;
            camera.Position = Vector2.Zero;
        }

        public void GenerateLevelOne()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            Tiles[1, 1] = new Tile(1, 1, 1, 1, false, true, false); FloorsLeft--;
            Tiles[1, 4] = new Tile(1, 4, 1, 1, false, true, false); FloorsLeft--;
            Tiles[1, 8] = new Tile(1, 8, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 2] = new Tile(2, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 3] = new Tile(2, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 5] = new Tile(2, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 6] = new Tile(2, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 4] = new Tile(3, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 7] = new Tile(3, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 4] = new Tile(4, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 7] = new Tile(4, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 7] = new Tile(5, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 3] = new Tile(7, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 4] = new Tile(7, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 5] = new Tile(7, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 6] = new Tile(7, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 1] = new Tile(8, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 8] = new Tile(8, 8, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(1 * 32, 1 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 1;
        }

        public void GenerateLevelTwo()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            Tiles[3, 1] = new Tile(3, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 1] = new Tile(4, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 1] = new Tile(5, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 2] = new Tile(6, 2, 1, 1, false, true, false); FloorsLeft--;
            Tiles[7, 2] = new Tile(7, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 3] = new Tile(6, 3, 1, 1, false, true, false); FloorsLeft--;
            Tiles[2, 4] = new Tile(2, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 4] = new Tile(5, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 5] = new Tile(2, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 5] = new Tile(4 , 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 6] = new Tile(2, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 6] = new Tile(3, 6, 1, 1, false, true, false); FloorsLeft--;
            Tiles[5, 6] = new Tile(5, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 7] = new Tile(2, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 7] = new Tile(5, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 7] = new Tile(7, 7, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(6 * 32, 3 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 2;
        }

        public void GenerateLevelThree()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            // Platforms
            Tiles[1, 5] = new Tile(1, 5, 1, 1, false, true, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 1, 1, false, true, false); FloorsLeft--;

            // Boost Pads
            Tiles[7, 2] = new Tile(7, 2, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[1, 4] = new Tile(1, 4, 2, 2, false, true, false, new Vector2(0f, -2f)); FloorsLeft--;
            Tiles[5, 4] = new Tile(5, 4, 0, 2, false, true, false, new Vector2(0f, 2f)); FloorsLeft--;
            Tiles[4, 5] = new Tile(4, 5, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[6, 5] = new Tile(6, 5, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[1, 6] = new Tile(1, 6, 2, 2, false, true, false, new Vector2(0f, -2f)); FloorsLeft--;
            Tiles[5, 6] = new Tile(5, 6, 0, 2, false, true, false, new Vector2(0f, 2f)); FloorsLeft--;
            Tiles[8, 7] = new Tile(8, 7, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[6, 8] = new Tile(6, 8, 2, 1, false, true, false, new Vector2(2f, 0f)); FloorsLeft--;

            // Holes
            Tiles[3, 1] = new Tile(3, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 1] = new Tile(7, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 1] = new Tile(8, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 2] = new Tile(8, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 3] = new Tile(4, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 3] = new Tile(6, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 4] = new Tile(2, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 4] = new Tile(3, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 4] = new Tile(4, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 4] = new Tile(6, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 6] = new Tile(2, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 6] = new Tile(4, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 6] = new Tile(6, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[2, 7] = new Tile(2, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 8] = new Tile(5, 8, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(5 * 32, 5 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 3;
        }

        public void GenerateLevelFour()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        //FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 3, 0, false, true, false, false);
                    }
                }
            }

            // Stone Tiles
            Tiles[1, 1] = new Tile(1, 1, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 1] = new Tile(5, 1, 0, 0, false, true, true); FloorsLeft++;
            Tiles[6, 1] = new Tile(6, 1, 0, 0, false, true, true); FloorsLeft++;
            Tiles[1, 2] = new Tile(1, 2, 0, 0, false, true, true); FloorsLeft++;
            Tiles[2, 2] = new Tile(2, 2, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 2] = new Tile(5, 2, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 3] = new Tile(5, 3, 0, 0, false, true, true); FloorsLeft++;
            Tiles[1, 4] = new Tile(1, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 4] = new Tile(5, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[7, 4] = new Tile(7, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[1, 5] = new Tile(1, 5, 0, 0, false, true, true); FloorsLeft++;
            Tiles[7, 5] = new Tile(7, 5, 0, 0, false, true, true); FloorsLeft++;
            Tiles[3, 6] = new Tile(3, 6, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 6] = new Tile(5, 6, 0, 0, false, true, true); FloorsLeft++;
            Tiles[2, 8] = new Tile(2, 8, 0, 0, false, true, true); FloorsLeft++;
            Tiles[3, 8] = new Tile(3, 8, 0, 0, false, true, true); FloorsLeft++;

            // Platforms
            Tiles[4, 1] = new Tile(4, 1, 1, 1, false, true, false);
            Tiles[8, 1] = new Tile(8, 1, 1, 1, false, true, false);
            Tiles[8, 6] = new Tile(8, 6, 1, 1, false, true, false);
            Tiles[4, 8] = new Tile(4, 8, 1, 1, false, true, false);
            Tiles[8, 8] = new Tile(8, 8, 1, 1, false, true, false);

            // Boost Pads
            Tiles[4, 2] = new Tile(4, 2, 0, 2, false, true, false, new Vector2(0f, 2f));
            Tiles[3, 4] = new Tile(3, 4, 1, 2, false, true, false, new Vector2(-2f, 0f));
            Tiles[7, 6] = new Tile(7, 6, 1, 2, false, true, false, new Vector2(-2f, 0f));

            Point spawn = new Point(4 * 32, 1 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 4;
        }

        public void GenerateLevelFive()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            // Platforms:
            Tiles[4, 3] = new Tile(4, 3, 1, 1, false, true, false); FloorsLeft--;
            Tiles[4, 5] = new Tile(4, 5, 1, 1, false, true, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 1, 1, false, true, false); FloorsLeft--;

            // Buttons:
            Tiles[2, 5] = new Tile(2, 5, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[5, 3] = new Tile(5, 3, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[8, 3] = new Tile(8, 3, 3, 1, false, true, false, false, true, true); FloorsLeft--;

            // Toggleable Floors
            // - Pink
            Tiles[6, 1] = new Tile(6, 1, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[4, 2] = new Tile(4, 2, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[5, 4] = new Tile(5, 4, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[3, 5] = new Tile(3, 5, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[4, 6] = new Tile(4, 6, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            // - Green
            Tiles[3, 1] = new Tile(3, 1, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[5, 2] = new Tile(5, 2, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[2, 4] = new Tile(2, 4, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[4, 4] = new Tile(4, 4, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[8, 4] = new Tile(8, 4, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[6, 5] = new Tile(6, 5, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[5, 6] = new Tile(5, 6, 2, 3, false, false, false, true, false, false); FloorsLeft--;

            // Holes
            Tiles[3, 2] = new Tile(3, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 2] = new Tile(6, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 3] = new Tile(1, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 3] = new Tile(3, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 3] = new Tile(6, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 6] = new Tile(3, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 6] = new Tile(6, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 7] = new Tile(3, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 8] = new Tile(6, 8, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(4 * 32, 5 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 5;
        }

        public void GenerateLevelSix()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            // Platforms:
            Tiles[1, 1] = new Tile(1, 1, 1, 1, false, true, false); FloorsLeft--;
            Tiles[7, 5] = new Tile(7, 5, 1, 1, false, true, false); FloorsLeft--;
            Tiles[8, 8] = new Tile(8, 8, 1, 1, false, true, false); FloorsLeft--;
            Tiles[6, 8] = new Tile(6, 8, 1, 1, false, true, false); FloorsLeft--;

            // Buttons:
            Tiles[8, 1] = new Tile(8, 1, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[1, 8] = new Tile(1, 8, 3, 1, false, true, false, false, true, true); FloorsLeft--;

            // Toggleable Tiles
            // - Pink
            Tiles[8, 2] = new Tile(8, 2, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[3, 5] = new Tile(3, 5, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[7, 8] = new Tile(7, 8, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            // - Green
            Tiles[7, 4] = new Tile(7, 4, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[8, 4] = new Tile(8, 4, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[1, 7] = new Tile(1, 7, 2, 3, false, false, false, true, false, false); FloorsLeft--;

            // Ice
            Tiles[4, 1] = new Tile(4, 1, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 1] = new Tile(5, 1, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 2] = new Tile(4, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 2] = new Tile(5, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 2] = new Tile(7, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 3] = new Tile(4, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 3] = new Tile(5, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 3] = new Tile(6, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 4] = new Tile(3, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 4] = new Tile(4, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 4] = new Tile(5, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 4] = new Tile(6, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 5] = new Tile(2, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 5] = new Tile(4, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 5] = new Tile(6, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 6] = new Tile(4, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 6] = new Tile(5, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 6] = new Tile(7, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 7] = new Tile(2, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 7] = new Tile(3, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 7] = new Tile(4, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 7] = new Tile(5, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 7] = new Tile(7, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 8] = new Tile(2, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 8] = new Tile(3, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 8] = new Tile(4, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 8] = new Tile(5, 8, 3, 0, false, true, false, false); FloorsLeft--;

            Point spawn = new Point(1 * 32, 1 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 6;
        }

        public void GenerateLevelSeven()
        {
            entityManager.Entities.Clear();

            width = 10;
            height = 10;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            // Platforms
            Tiles[1, 1] = new Tile(1, 1, 1, 1, false, true, false); FloorsLeft--;
            Tiles[1, 2] = new Tile(1, 2, 1, 1, false, true, false); FloorsLeft--;
            Tiles[6, 5] = new Tile(6, 5, 1, 1, false, true, false); FloorsLeft--;
            Tiles[4, 6] = new Tile(4, 6, 1, 1, false, true, false); FloorsLeft--;

            // Buttons
            Tiles[8, 8] = new Tile(8, 8, 3, 1, false, true, false, false, true, true); FloorsLeft--;

            // Boost Pads
            Tiles[2, 2] = new Tile(2, 2, 0, 2, false, true, false, new Vector2(0f, 2f)); FloorsLeft--;
            Tiles[7, 5] = new Tile(7, 5, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[2, 7] = new Tile(2, 7, 2, 1, false, true, false, new Vector2(2f, 0f)); FloorsLeft--;

            // Toggleable Platforms
            // - Pink
            Tiles[2, 6] = new Tile(2, 6, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            // - Green
            Tiles[2, 1] = new Tile(2, 1, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[4, 3] = new Tile(4, 3, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[8, 7] = new Tile(8, 7, 2, 3, false, false, false, true, false, false); FloorsLeft--;

            // Ice
            Tiles[5, 2] = new Tile(5, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 2] = new Tile(3, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 2] = new Tile(7, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[8, 2] = new Tile(8, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[1, 3] = new Tile(1, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 3] = new Tile(2, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 4] = new Tile(2, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 4] = new Tile(5, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 4] = new Tile(6, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 4] = new Tile(7, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[8, 4] = new Tile(8, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 5] = new Tile(2, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 6] = new Tile(6, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 6] = new Tile(7, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 7] = new Tile(3, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[3, 8] = new Tile(3, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[4, 8] = new Tile(4, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 8] = new Tile(5, 8, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 8] = new Tile(6, 8, 3, 0, false, true, false, false); FloorsLeft--;

            // Holes
            Tiles[4, 1] = new Tile(4, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 1] = new Tile(5, 1, 0, 1, false, false, false); FloorsLeft--; 
            Tiles[5, 7] = new Tile(5, 7, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(1 * 32, 1 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 7;
        }

        public void GenerateLevelEight()
        {
            entityManager.Entities.Clear();

            width = 14;
            height = 14;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Stone Tiles
                    else
                    {
                        FloorsLeft++;
                        Tiles[x, y] = new Tile(x, y, 0, 0, false, true, true);
                    }
                }
            }

            // Platforms
            Tiles[2, 3] = new Tile(2, 3, 1, 1, false, true, false); FloorsLeft--;
            Tiles[12, 3] = new Tile(12, 3, 1, 1, false, true, false); FloorsLeft--;
            Tiles[8, 4] = new Tile(8, 4, 1, 1, false, true, false); FloorsLeft--;
            Tiles[6, 6] = new Tile(6, 6, 1, 1, false, true, false); FloorsLeft--;
            Tiles[6, 7] = new Tile(6, 7, 1, 1, false, true, false); FloorsLeft--;
            Tiles[7, 6] = new Tile(7, 6, 1, 1, false, true, false); FloorsLeft--;
            Tiles[7, 7] = new Tile(7, 7, 1, 1, false, true, false); FloorsLeft--;
            Tiles[2, 11] = new Tile(2, 11, 1, 1, false, true, false); FloorsLeft--;

            // Buttons
            Tiles[3, 2] = new Tile(3, 2, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[11, 3] = new Tile(11, 3, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[9, 8] = new Tile(9, 8, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[2, 10] = new Tile(2, 10, 3, 1, false, true, false, false, true, true); FloorsLeft--;
            Tiles[10, 11] = new Tile(10, 11, 3, 1, false, true, false, false, true, true); FloorsLeft--;

            // Toggleable Platforms
            // - Pink
            Tiles[6, 4] = new Tile(6, 4, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[4, 7] = new Tile(4, 7, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[9, 7] = new Tile(9, 7, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            Tiles[7, 9] = new Tile(7, 9, 1, 3, false, true, false, true, false, true); FloorsLeft--;
            // - Green
            Tiles[4, 6] = new Tile(4, 6, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[9, 6] = new Tile(9, 6, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[6, 9] = new Tile(6, 9, 2, 3, false, false, false, true, false, false); FloorsLeft--;
            Tiles[5, 11] = new Tile(5, 11, 2, 3, false, false, false, true, false, false); FloorsLeft--;

            // Boost Pads
            Tiles[6, 5] = new Tile(6, 5, 2, 2, false, true, false, new Vector2(0f, -2f)); FloorsLeft--;
            Tiles[7, 5] = new Tile(7, 5, 0, 2, false, true, false, new Vector2(0f, 2f)); FloorsLeft--;
            Tiles[5, 6] = new Tile(5, 6, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[8, 6] = new Tile(8, 6, 2, 1, false, true, false, new Vector2(2f, 0f)); FloorsLeft--;
            Tiles[5, 7] = new Tile(5, 7, 2, 1, false, true, false, new Vector2(2f, 0f)); FloorsLeft--;
            Tiles[8, 7] = new Tile(8, 7, 1, 2, false, true, false, new Vector2(-2f, 0f)); FloorsLeft--;
            Tiles[6, 8] = new Tile(6, 8, 2, 2, false, true, false, new Vector2(0f, -2f)); FloorsLeft--;
            Tiles[7, 8] = new Tile(7, 8, 0, 2, false, true, false, new Vector2(0f, 2f)); FloorsLeft--;

            // Ice
            Tiles[2, 2] = new Tile(2, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[5, 2] = new Tile(5, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 2] = new Tile(6, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 2] = new Tile(7, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[11, 2] = new Tile(11, 2, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[8, 3] = new Tile(8, 3, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 4] = new Tile(7, 4, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 6] = new Tile(2, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[12, 6] = new Tile(12, 6, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[2, 7] = new Tile(2, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[12, 7] = new Tile(12, 7, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[6, 11] = new Tile(6, 11, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[7, 11] = new Tile(7, 11, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[8, 11] = new Tile(8, 11, 3, 0, false, true, false, false); FloorsLeft--;
            Tiles[11, 11] = new Tile(11, 11, 3, 0, false, true, false, false); FloorsLeft--;

            // Holes
            Tiles[1, 1] = new Tile(1, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 1] = new Tile(6, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 1] = new Tile(7, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[12, 1] = new Tile(12, 1, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 2] = new Tile(1, 2, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 3] = new Tile(3, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 3] = new Tile(4, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 3] = new Tile(6, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 3] = new Tile(7, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[9, 3] = new Tile(9, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 3] = new Tile(10, 3, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 4] = new Tile(4, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 4] = new Tile(10, 4, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 5] = new Tile(5, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 5] = new Tile(8, 5, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 6] = new Tile(1, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 6] = new Tile(3, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 6] = new Tile(10, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[11, 6] = new Tile(11, 6, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 7] = new Tile(1, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 7] = new Tile(3, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 7] = new Tile(10, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[11, 7] = new Tile(11, 7, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 8] = new Tile(5, 8, 0, 1, false, false, false); FloorsLeft--;
            Tiles[8, 8] = new Tile(8, 8, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 9] = new Tile(4, 9, 0, 1, false, false, false); FloorsLeft--;
            Tiles[9, 9] = new Tile(9, 9, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 9] = new Tile(10, 9, 0, 1, false, false, false); FloorsLeft--;
            Tiles[3, 10] = new Tile(3, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[4, 10] = new Tile(4, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 10] = new Tile(6, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 10] = new Tile(7, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[9, 10] = new Tile(9, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[10, 10] = new Tile(10, 10, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 11] = new Tile(1, 11, 0, 1, false, false, false); FloorsLeft--;
            Tiles[1, 12] = new Tile(1, 12, 0, 1, false, false, false); FloorsLeft--;
            Tiles[5, 12] = new Tile(5, 12, 0, 1, false, false, false); FloorsLeft--;
            Tiles[6, 12] = new Tile(6, 12, 0, 1, false, false, false); FloorsLeft--;
            Tiles[7, 12] = new Tile(7, 12, 0, 1, false, false, false); FloorsLeft--;
            Tiles[12, 12] = new Tile(12, 12, 0, 1, false, false, false); FloorsLeft--;

            Point spawn = new Point(7 * 32, 7 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 8;
        }

        public void GenerateLevelNine()
        {
            entityManager.Entities.Clear();

            width = 14;
            height = 14;
            FloorsLeft = 0;
            Tiles = new Tile[width, height];
            texture = content.Load<Texture2D>("tiles");

            Vector2 screenCenter = new Vector2(((width * 32) - 800) / 2, ((height * 32) - 480) / 2);
            camera.Position = screenCenter;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Create Boarder
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        Tiles[x, y] = new Tile(x, y, 0, 1, false, false, false);
                    }
                    // Create Ice Tiles
                    else
                    {
                        Tiles[x, y] = new Tile(x, y, 3, 0, false, true, false, false);
                    }
                }
            }

            // Stone Tiles
            Tiles[5, 1] = new Tile(5, 1, 0, 0, false, true, true); FloorsLeft++;
            Tiles[8, 1] = new Tile(8, 1, 0, 0, false, true, true); FloorsLeft++;
            Tiles[6, 4] = new Tile(6, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[7, 4] = new Tile(7, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[11, 4] = new Tile(11, 4, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 5] = new Tile(5, 5, 0, 0, false, true, true); FloorsLeft++;
            Tiles[8, 5] = new Tile(8, 5, 0, 0, false, true, true); FloorsLeft++;
            Tiles[1, 6] = new Tile(1, 6, 0, 0, false, true, true); FloorsLeft++;
            Tiles[5, 6] = new Tile(5, 6, 0, 0, false, true, true); FloorsLeft++;
            Tiles[8, 6] = new Tile(8, 6, 0, 0, false, true, true); FloorsLeft++;
            Tiles[2, 7] = new Tile(2, 7, 0, 0, false, true, true); FloorsLeft++;
            Tiles[6, 7] = new Tile(6, 7, 0, 0, false, true, true); FloorsLeft++;
            Tiles[7, 7] = new Tile(7, 7, 0, 0, false, true, true); FloorsLeft++;
            Tiles[11, 7] = new Tile(11, 7, 0, 0, false, true, true); FloorsLeft++;
            Tiles[11, 8] = new Tile(11, 8, 0, 0, false, true, true); FloorsLeft++;
            Tiles[12, 8] = new Tile(12, 8, 0, 0, false, true, true); FloorsLeft++;
            Tiles[1, 12] = new Tile(1, 12, 0, 0, false, true, true); FloorsLeft++;
            Tiles[12, 12] = new Tile(12, 12, 0, 0, false, true, true); FloorsLeft++;

            // Platforms
            Tiles[7, 1] = new Tile(7, 1, 1, 1, false, true, false);
            Tiles[5, 2] = new Tile(5, 2, 1, 1, false, true, false);
            Tiles[11, 3] = new Tile(11, 3, 1, 1, false, true, false);
            Tiles[1, 11] = new Tile(1, 11, 1, 1, false, true, false);
            Tiles[12, 11] = new Tile(12, 11, 1, 1, false, true, false);
            Tiles[6, 12] = new Tile(6, 12, 1, 1, false, true, false);
            Tiles[7, 12] = new Tile(7, 12, 1, 1, false, true, false);

            // Buttons
            Tiles[1, 1] = new Tile(1, 1, 3, 1, false, true, false, false, true, true);
            Tiles[12, 1] = new Tile(12, 1, 3, 1, false, true, false, false, true, true);

            // Toggleable Platforms
            // - Pink
            Tiles[12, 4] = new Tile(12, 4, 1, 3, false, true, false, true, false, true); 
            Tiles[11, 5] = new Tile(11, 5, 1, 3, false, true, false, true, false, true); 
            Tiles[11, 6] = new Tile(11, 6, 1, 3, false, true, false, true, false, true); 
            Tiles[12, 7] = new Tile(12, 7, 1, 3, false, true, false, true, false, true); 
            Tiles[5, 12] = new Tile(5, 12, 1, 3, false, true, false, true, false, true); 
            // - Green
            Tiles[8, 12] = new Tile(8, 12, 2, 3, false, false, false, true, false, false);

            // Boost Pads
            Tiles[2, 1] = new Tile(2, 1, 0, 2, false, true, false, new Vector2(0f, 2f)); 
            Tiles[4, 1] = new Tile(4, 1, 0, 2, false, true, false, new Vector2(0f, 2f)); 
            Tiles[9, 1] = new Tile(9, 1, 0, 2, false, true, false, new Vector2(0f, 2f)); 
            Tiles[10, 1] = new Tile(10, 1, 0, 2, false, true, false, new Vector2(0f, 2f));
            Tiles[1, 2] = new Tile(1, 2, 2, 2, false, true, false, new Vector2(0f, -2f)); 
            Tiles[8, 2] = new Tile(8, 2, 2, 1, false, true, false, new Vector2(2f, 0f)); 
            Tiles[12, 2] = new Tile(12, 2, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[1, 3] = new Tile(1, 3, 0, 2, false, true, false, new Vector2(0f, 2f));
            Tiles[2, 3] = new Tile(2, 3, 1, 2, false, true, false, new Vector2(-2f, 0f));
            Tiles[6, 3] = new Tile(6, 3, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[2, 4] = new Tile(2, 4, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[5, 4] = new Tile(5, 4, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[8, 4] = new Tile(8, 4, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[10, 5] = new Tile(10, 5, 1, 2, false, true, false, new Vector2(-2f, 0f));
            Tiles[2, 6] = new Tile(2, 6, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[4, 6] = new Tile(4, 6, 0, 2, false, true, false, new Vector2(0f, 2f));
            Tiles[1, 7] = new Tile(1, 7, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[3, 7] = new Tile(3, 7, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[5, 7] = new Tile(5, 7, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[8, 7] = new Tile(8, 7, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[10, 7] = new Tile(10, 7, 1, 2, false, true, false, new Vector2(-2f, 0f));
            Tiles[1, 10] = new Tile(1, 10, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[2, 11] = new Tile(2, 11, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[5, 11] = new Tile(5, 11, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[8, 11] = new Tile(8, 11, 2, 1, false, true, false, new Vector2(2f, 0f));
            Tiles[4, 12] = new Tile(4, 12, 1, 2, false, true, false, new Vector2(-2f, 0f));
            Tiles[8, 12] = new Tile(8, 12, 2, 2, false, true, false, new Vector2(0f, -2f));
            Tiles[10, 12] = new Tile(10, 12, 2, 2, false, true, false, new Vector2(0f, -2f));

            // Holes
            Tiles[6, 1] = new Tile(6, 1, 0, 1, false, false, false); 
            Tiles[6, 2] = new Tile(6, 2, 0, 1, false, false, false); 
            Tiles[12, 3] = new Tile(12, 3, 0, 1, false, false, false);

            Point spawn = new Point(7 * 32, 12 * 32);
            entityManager.AddPlayer(spawn.X, spawn.Y);
            PlayingLevel = true;
            CurrentLevel = 9;
        }

        public bool HasCollision(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.IsCollidable;
            }
            return false;
        }

        public bool HasFloor(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.IsFloor;
            }
            return false;
        }

        public bool HasVelocity(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.HasVelocity;
            }
            return false;
        }

        public bool HasFriction(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.HasFriction;
            }
            return false;
        }

        public bool IsButton(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.IsButton;
            }
            return false;
        }

        public void ToggleFloors()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Tile tile = Get(x, y);
                    if (tile != null)
                    {
                        if (tile.IsToggleable)
                        {
                            if (tile.IsEnabled)
                            {
                                tile.Set(tile.U - 1, tile.V, false, false, false);
                            }
                            else if (!tile.IsEnabled)
                            {
                                tile.Set(tile.U + 1, tile.V, false, true, true);
                            }
                        }
                    }
                }
            }
        }

        public Vector2 GetVelocity(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.Velocity;
            }
            return Vector2.Zero;
        }

        public bool CanBreak(int x, int y)
        {
            Tile tile = Get(x, y);
            if (tile != null)
            {
                return tile.FloorCanBreak;
            }
            return false;
        }

        public void DestroyFloor(int x, int y)
        {
            if (CanBreak(x, y))
            {
                Set(x, y, 0, 1, false, false);
                Get(x, y).FloorCanBreak = false;
                FloorsLeft--;
            }
        }

        public void DamageFloor(int x, int y)
        {
            if (CanBreak(x, y))
            {
                Set(x, y, 1, 0, false, true);
            }
        }

        public Tile Get(int x, int y)
        {
            if (x >= 0 && x < width && y >= 0 && y < height)
            {
                return Tiles[x, y];
            }
            return null;
        }

        public void Set(int x, int y, int u, int v, bool isCollidable, bool isFloor)
        {
            if (x >= 0 && x < width && y >= 0 && y < height)
            {
                Tiles[x, y].Set(u, v, isCollidable, isFloor);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (PlayingLevel)
            {
                foreach (Tile tile in Tiles)
                {
                    tile.Draw(spriteBatch, texture);
                }
            } 
        }
    }
}
