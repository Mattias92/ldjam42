﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42.World
{
    public class Tile
    {
        private int width = 32;
        private int height = 32;

        public int X { get; set; }
        public int Y { get; set; }
        public int U { get; set; }
        public int V { get; set; }

        public bool IsCollidable { get; set; }
        public bool IsFloor { get; set; }
        public bool FloorCanBreak { get; set; }

        public bool HasVelocity { get; set; }
        public bool HasFriction { get; set; }
        public Vector2 Velocity { get; set; }

        public bool IsToggleable { get; set; }
        public bool IsButton { get; set; }
        public bool IsEnabled { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle(X, Y, X * width, Y * height);
            }
            private set { }
        }

        public Tile(int x, int y, int u, int v, bool isColliable, bool isFloor, bool floorCanBreak)
        {
            X = x;
            Y = y;
            U = u;
            V = v;
            IsCollidable = isColliable;
            IsFloor = isFloor;
            FloorCanBreak = floorCanBreak;
            HasFriction = true;
            HasVelocity = false;
            Velocity = Vector2.Zero;
            
            IsToggleable = false;
            IsButton = false;
            IsEnabled = true;
        }

        public Tile(int x, int y, int u, int v, bool isColliable, bool isFloor, bool floorCanBreak, Vector2 velocity)
        {
            X = x;
            Y = y;
            U = u;
            V = v;
            IsCollidable = isColliable;
            IsFloor = isFloor;
            FloorCanBreak = floorCanBreak;
            HasFriction = true;
            HasVelocity = true;
            Velocity = velocity;
            IsToggleable = false;
            IsButton = false;
            IsEnabled = true;
        }

        public Tile(int x, int y, int u, int v, bool isColliable, bool isFloor, bool floorCanBreak, bool hasFriction)
        {
            X = x;
            Y = y;
            U = u;
            V = v;
            IsCollidable = isColliable;
            IsFloor = isFloor;
            FloorCanBreak = floorCanBreak;
            HasFriction = hasFriction;
            HasVelocity = false;
            Velocity = Vector2.Zero;
            IsToggleable = false;
            IsButton = false;
            IsEnabled = true;
        }

        public Tile(int x, int y, int u, int v, bool isColliable, bool isFloor, bool floorCanBreak, bool isToggleable, bool isButton, bool isEnabled)
        {
            X = x;
            Y = y;
            U = u;
            V = v;
            IsCollidable = isColliable;
            IsFloor = isFloor;
            FloorCanBreak = floorCanBreak;
            HasFriction = true;
            HasVelocity = false;
            Velocity = Vector2.Zero;
            IsToggleable = isToggleable;
            IsButton = isButton;
            IsEnabled = isEnabled;
        }

        public void Set(int u, int v, bool isCollidable, bool isFloor)
        {
            U = u;
            V = v;
            IsCollidable = isCollidable;
            IsFloor = isFloor;
        }

        public void Set(int u, int v, bool isCollidable, bool isFloor, bool isEnabled)
        {
            U = u;
            V = v;
            IsCollidable = isCollidable;
            IsFloor = isFloor;
            IsEnabled = isEnabled;
        }

        public override Boolean Equals(System.Object obj)
        {
            var t = obj as Tile;
            if (obj == null)
            {
                return false;
            }
            return X.Equals(t.X) && Y.Equals(t.Y);
        }

        public override int GetHashCode()
        {
            return (((X << 16) | (X >> 16)) ^ Y).GetHashCode();
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D texture)
        {
            spriteBatch.Draw(texture, new Vector2(X * width, Y * height), new Rectangle(U * width, V * height, width, height), Color.White);
        }
    }
}
