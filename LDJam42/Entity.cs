﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam42
{
    public class Entity
    {
        public int Id { get; set; }
        public Dictionary<Type, Component> Components { get; set; }
        public bool Killed { get; set; }

        public Entity(int id)
        {
            Id = id;
            Components = new Dictionary<Type, Component>();
            Killed = false;
        }

        public void AddComponent(Component component)
        {
            Components[component.GetType()] = component;
        }

        public void RemoveComponent(Component component)
        {
            Components.Remove(component.GetType());
        }

        public T GetComponent<T>() where T : Component
        {
            if (!Components.ContainsKey(typeof(T)))
            {
                return null;
            }
            return (T)Components[typeof(T)];
        }

        public bool HasComponent<T>() where T : Component
        {
            return Components.ContainsKey(typeof(T));
        }
    }
}
